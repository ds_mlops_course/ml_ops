import pandas as pd
import numpy as np
from tqdm.notebook import tqdm

def __df_year_month(start: str, end: str):
    dates_df = pd.DataFrame(
        {
            "date": pd.date_range(
                start=start, end=end + pd.DateOffset(months=1), freq="M"
            )
        }
    )

    dates_df["date"] = dates_df["date"].apply(lambda row: row.replace(day=1))
    dates_df["year"] = dates_df["date"].dt.year
    dates_df["month"] = dates_df["date"].dt.month

    return dates_df


def create_unique_combination_column(
    input_df: pd.DataFrame, columns_for_ts_combination_list: list
):
    if len(columns_for_ts_combination_list) > 1:
        return input_df[columns_for_ts_combination_list].apply(
            lambda row: "|".join(row), axis=1
        )
    else:
        return input_df[columns_for_ts_combination_list[0]]


def drop_na_and_nunique(x: pd.DataFrame):
    return x.dropna().nunique()


def calculate_all(
    sales_df: pd.DataFrame,
    unique_combination_df: pd.DataFrame,
    unique_combination_list: list,
    calculate_rr,
):
    columns_for_nunique_list = ["n_cust", "orders"]
    columns_for_sum_list = [
        "turnover",
        "product_revenue",
        "delivery",
        "safe_order",
        "surcharge",
        "pledge",
        "expert_check",
        "margin",
        "products_quantity",
        "turnover_ar",
    ]
    columns_dropna_and_nunique_list = ["cust_with_refunds", "orders_with_refunds"]

    all_combination_list = []
    for unique_combination in tqdm(
        unique_combination_df["unique_combination"].unique()
    ):
        print(unique_combination)
        combination_sales_df = sales_df[
            sales_df["unique_combination"] == unique_combination
        ].reset_index(drop=True)
        combination_sales_df["is_total"] = 1

        combination_sales_df.rename(
            columns={"customer_id": "n_cust", "order_id": "orders"}, inplace=True
        )

        selected_uncb_results_df = __df_year_month(
            start=sales_df["date_created"].min(), end=sales_df["date_created"].max()
        ).copy()
        selected_uncb_results_df["unique_combination"] = unique_combination
        for ind, col in enumerate(unique_combination_list):
            selected_uncb_results_df[col] = unique_combination.split("|")[ind]

        for cust_type in ["act", "react", "new", "total"]:
            df = (
                combination_sales_df[combination_sales_df[f"is_{cust_type}"] == 1]
                .groupby(["year", "month"])
                .agg(
                    **{
                        **{
                            f"{col}_{cust_type}": pd.NamedAgg(column=col, aggfunc="sum")
                            for col in columns_for_sum_list
                        },
                        **{
                            f"{col}_{cust_type}": pd.NamedAgg(
                                column=col, aggfunc="nunique"
                            )
                            for col in columns_for_nunique_list
                        },
                        **{
                            f"{col}_{cust_type}": pd.NamedAgg(
                                column=col, aggfunc=drop_na_and_nunique
                            )
                            for col in columns_dropna_and_nunique_list
                        },
                        f"unique_customers_{cust_type}": pd.NamedAgg(
                            column="n_cust", aggfunc="unique"
                        ),
                    }
                )
                .reset_index()
            )

            if df.empty:
                df = pd.DataFrame(
                    columns=["year", "month"]
                    + [
                        f"{col}_{cust_type}"
                        for col in (
                            columns_for_sum_list
                            + columns_for_nunique_list
                            + columns_dropna_and_nunique_list
                            + [f"unique_customers"]
                        )
                    ]
                )

            selected_uncb_results_df = selected_uncb_results_df.merge(
                df, how="outer", on=["year", "month"]
            )

        for col in [
            f"unique_customers_{cust_type}"
            for cust_type in ["act", "react", "new", "total"]
        ]:
            selected_uncb_results_df[col] = [
                np.array([]) if _ is np.NaN else _
                for _ in selected_uncb_results_df[col]
            ]

        selected_uncb_results_df = selected_uncb_results_df.fillna(0)

        for cust_type in ["act", "react", "new", "total"]:
            selected_uncb_results_df[f"freq_base_{cust_type}"] = (
                selected_uncb_results_df[f"orders_{cust_type}"]
                / selected_uncb_results_df[f"n_cust_{cust_type}"]
            )
            selected_uncb_results_df[f"AOV_{cust_type}"] = (
                selected_uncb_results_df[f"turnover_{cust_type}"]
                / selected_uncb_results_df[f"orders_{cust_type}"]
            )

        for target_prefix in ["turnover", "orders", "n_cust"]:
            if (
                np.abs(
                    selected_uncb_results_df[f"{target_prefix}_act"]
                    + selected_uncb_results_df[f"{target_prefix}_react"]
                    + selected_uncb_results_df[f"{target_prefix}_new"]
                    - selected_uncb_results_df[f"{target_prefix}_total"]
                )
            ).sum() > 1:
                raise Exception(f"Diff in {target_prefix}")

        if calculate_rr:
            rr_freq_df = selected_uncb_results_df[
                ["year", "month"]
                + [
                    f"unique_customers_{cust_type}"
                    for cust_type in ["act", "react", "new", "total"]
                ]
            ].copy()

            rr_freq_df["n_cust_other"] = np.NaN
            rr_freq_df["orders_other"] = np.NaN
            rr_freq_df["freq_other"] = np.NaN
            rr_freq_df["freq_other"] = np.NaN
            rr_freq_df["turnover_other"] = np.NaN
            rr_freq_df["AOV_other"] = np.NaN

            for cust_type in ["act", "react", "new", "total"]:
                for month_rr_num in range(1, 13):
                    rr_freq_df[f"cohort_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"RR_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"orders_cohort_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"freq_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"turnover_cohort_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"AOV_cohort_{month_rr_num}_{cust_type}"] = np.NaN

            for row_index in range(12, rr_freq_df.shape[0]):
                slice_country_cust_df = (
                    rr_freq_df[
                        ["year", "month"]
                        + [
                            f"unique_customers_{cust_type}"
                            for cust_type in ["act", "react", "new", "total"]
                        ]
                    ]
                    .iloc[row_index - 12 : row_index + 1]
                    .reset_index(drop=True)
                )

                selected_year = slice_country_cust_df.iloc[-1]["year"]
                selected_month = slice_country_cust_df.iloc[-1]["month"]

                unique_cust_orders_df = combination_sales_df[
                    ["n_cust", "orders", "turnover"]
                ][
                    (combination_sales_df["year"] == selected_year)
                    & (combination_sales_df["month"] == selected_month)
                ].reset_index(
                    drop=True
                )

                target_rr_month_year_cust_id = slice_country_cust_df[
                    "unique_customers_act"
                ].iloc[-1]

                inluded_cust_type_list = []
                for cust_type in ["act", "react", "new"]:
                    slice_country_cust_df[f"intersect_with_target_{cust_type}"] = np.NaN
                    slice_country_cust_df[
                        f"intersect_with_target_{cust_type}"
                    ] = slice_country_cust_df[
                        f"intersect_with_target_{cust_type}"
                    ].astype(
                        object
                    )
                    slice_country_cust_df[f"n_all_{cust_type}"] = np.NaN
                    slice_country_cust_df[f"cohort_{cust_type}"] = np.NaN
                    slice_country_cust_df[f"orders_cohort_{cust_type}"] = np.NaN
                    slice_country_cust_df[f"turnover_cohort_{cust_type}"] = np.NaN

                    for ind in range(slice_country_cust_df.shape[0] - 2, -1, -1):
                        cust_in_previos_period = slice_country_cust_df[
                            f"unique_customers_{cust_type}"
                        ].iloc[ind]
                        itersection_arr = np.intersect1d(
                            target_rr_month_year_cust_id, cust_in_previos_period
                        )

                        itersection_list = list(
                            set(itersection_arr) - set(inluded_cust_type_list)
                        )
                        inluded_cust_type_list.extend(itersection_arr)

                        # Sometimes strange error raised that cant set row with value
                        # But value is replaced. So just added "pass" to ignore this error
                        try:
                            slice_country_cust_df[
                                f"intersect_with_target_{cust_type}"
                            ].iloc[ind] = itersection_list
                        except ValueError:
                            pass

                        slice_country_cust_df[f"n_all_{cust_type}"].iloc[
                            ind
                        ] = cust_in_previos_period.shape[0]
                        slice_country_cust_df[f"cohort_{cust_type}"].iloc[ind] = len(
                            itersection_list
                        )

                        cohort_orders_df = unique_cust_orders_df[
                            unique_cust_orders_df["n_cust"].isin(itersection_list)
                        ]
                        slice_country_cust_df[f"orders_cohort_{cust_type}"].iloc[
                            ind
                        ] = cohort_orders_df["orders"].shape[0]
                        slice_country_cust_df[f"turnover_cohort_{cust_type}"].iloc[
                            ind
                        ] = cohort_orders_df["turnover"].sum()

                slice_country_cust_df["n_all_total"] = slice_country_cust_df[
                    ["n_all_act", "n_all_react", "n_all_new"]
                ].sum(axis=1)
                slice_country_cust_df["orders_cohort_total"] = slice_country_cust_df[
                    ["orders_cohort_act", "orders_cohort_react", "orders_cohort_new"]
                ].sum(axis=1)
                slice_country_cust_df["cohort_total"] = slice_country_cust_df[
                    ["cohort_act", "cohort_react", "cohort_new"]
                ].sum(axis=1)
                slice_country_cust_df["turnover_cohort_total"] = slice_country_cust_df[
                    [
                        "turnover_cohort_act",
                        "turnover_cohort_react",
                        "turnover_cohort_new",
                    ]
                ].sum(axis=1)

                for cust_type in ["act", "react", "new", "total"]:
                    slice_country_cust_df[f"RR_{cust_type}"] = (
                        slice_country_cust_df[f"cohort_{cust_type}"]
                        / slice_country_cust_df[f"n_all_{cust_type}"]
                    ).fillna(0)
                    slice_country_cust_df[f"freq_{cust_type}"] = (
                        slice_country_cust_df[f"orders_cohort_{cust_type}"]
                        / slice_country_cust_df[f"cohort_{cust_type}"]
                    )
                    slice_country_cust_df[f"AOV_cohort_{cust_type}"] = (
                        slice_country_cust_df[f"turnover_cohort_{cust_type}"]
                        / slice_country_cust_df[f"orders_cohort_{cust_type}"]
                    )

                # Some customers for FR come from DE, IT etc
                # And as they dont return from any cohort from FR
                # We create separate clients
                cust_from_other_uncb = list(
                    set(target_rr_month_year_cust_id) - set(inluded_cust_type_list)
                )
                n_cust_from_other_uncb = len(cust_from_other_uncb)

                other_cohort_orders = unique_cust_orders_df[
                    unique_cust_orders_df["n_cust"].isin(cust_from_other_uncb)
                ]
                orders_from_other_uncb = other_cohort_orders["orders"].shape[0]
                turnover_from_other_uncb = other_cohort_orders["turnover"].sum()

                if (n_cust_from_other_uncb > 0) & (orders_from_other_uncb >= 0):
                    freq_from_other_uncb = (
                        orders_from_other_uncb / n_cust_from_other_uncb
                    )
                else:
                    freq_from_other_uncb = np.NaN

                if (turnover_from_other_uncb > 0) & (orders_from_other_uncb >= 0):
                    aov_from_other_uncb = (
                        turnover_from_other_uncb / orders_from_other_uncb
                    )
                else:
                    aov_from_other_uncb = np.NaN

                # Insert result from slice into main DataFrame
                slice_results = slice_country_cust_df[
                    [
                        f"{target}_{cust_type}"
                        for cust_type in ["act", "react", "new", "total"]
                        for target in [
                            "cohort",
                            "RR",
                            "orders_cohort",
                            "freq",
                            "turnover_cohort",
                            "AOV_cohort",
                        ]
                    ]
                ].iloc[:-1]

                slice_results = slice_results.iloc[::-1]
                slice_results.index = [month_before for month_before in range(1, 13)]

                index_for_replacing = (rr_freq_df["year"] == selected_year) & (
                    rr_freq_df["month"] == selected_month
                )
                rr_freq_df["n_cust_other"][index_for_replacing] = n_cust_from_other_uncb
                rr_freq_df["orders_other"][index_for_replacing] = orders_from_other_uncb
                rr_freq_df["freq_other"][index_for_replacing] = freq_from_other_uncb
                rr_freq_df["turnover_other"][
                    index_for_replacing
                ] = turnover_from_other_uncb
                rr_freq_df["AOV_other"][index_for_replacing] = aov_from_other_uncb

                for column in slice_results.columns:
                    for month_before in slice_results[column].index:
                        columns_for_insert = f"{'_'.join(column.split('_')[:-1])}_{month_before}_{column.split('_')[-1]}"
                        rr_freq_df[columns_for_insert][
                            index_for_replacing
                        ] = slice_results[column].loc[month_before]

            # Merge turnover, orders etc DataFrame with split Active customers RR, freq, orders by cohort
            selected_uncb_results_df = selected_uncb_results_df.drop(
                columns=[
                    f"unique_customers_{cust_type}"
                    for cust_type in ["act", "react", "new", "total"]
                ]
            ).merge(
                rr_freq_df.drop(
                    columns=[
                        f"unique_customers_{cust_type}"
                        for cust_type in ["act", "react", "new", "total"]
                    ]
                ),
                how="left",
                on=["year", "month"],
            )

            selected_uncb_results_df = selected_uncb_results_df[
                selected_uncb_results_df["year"] >= 2016
            ].reset_index(drop=True)

            if (
                selected_uncb_results_df["n_cust_act"]
                - selected_uncb_results_df[
                    ["n_cust_other"]
                    + [
                        _
                        for _ in selected_uncb_results_df.columns
                        if ("cohort" in _)
                        and ("orders_cohort" not in _)
                        and ("_total" not in _)
                    ]
                ].sum(axis=1)
                > 0
            ).any():
                raise Exception("N active customers total != sum from cohort ")

            if (
                selected_uncb_results_df["orders_act"]
                - selected_uncb_results_df[
                    ["orders_other"]
                    + [
                        _
                        for _ in selected_uncb_results_df.columns
                        if ("orders_cohort" in _) and ("_total" not in _)
                    ]
                ].sum(axis=1)
                > 0
            ).any():
                raise Exception(
                    "N orders from active customers total != sum from cohort "
                )

            if (
                selected_uncb_results_df["turnover_act"]
                - selected_uncb_results_df[
                    ["turnover_other"]
                    + [
                        _
                        for _ in selected_uncb_results_df.columns
                        if ("turnover_cohort" in _) and ("_total" not in _)
                    ]
                ].sum(axis=1)
                > 1
            ).any():
                raise Exception(
                    "Turnover from active customers total != sum from cohort "
                )

            if (
                (
                    selected_uncb_results_df[
                        [
                            _
                            for _ in selected_uncb_results_df.columns
                            if "AOV_cohort" in _
                        ]
                    ]
                    < 0
                )
                .any()
                .any()
            ):
                raise Exception("Some AOV < 0")

            if (
                (
                    selected_uncb_results_df[
                        [_ for _ in selected_uncb_results_df.columns if "RR" in _]
                    ]
                    < 0
                )
                .any()
                .any()
            ):
                raise Exception("Some RR < 0")

            if (
                (
                    selected_uncb_results_df[
                        [_ for _ in selected_uncb_results_df.columns if "RR" in _]
                    ]
                    > 1
                )
                .any()
                .any()
            ):
                raise Exception("Some RR > 1")
        else:
            rr_freq_df = selected_uncb_results_df[
                ["year", "month"]
                + [
                    f"unique_customers_{cust_type}"
                    for cust_type in ["act", "react", "new", "total"]
                ]
            ].copy()

            rr_freq_df["n_cust_other"] = np.NaN
            rr_freq_df["orders_other"] = np.NaN
            rr_freq_df["freq_other"] = np.NaN
            rr_freq_df["freq_other"] = np.NaN
            rr_freq_df["turnover_other"] = np.NaN
            rr_freq_df["AOV_other"] = np.NaN

            for cust_type in ["act", "react", "new", "total"]:
                for month_rr_num in range(1, 13):
                    rr_freq_df[f"cohort_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"RR_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"orders_cohort_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"freq_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"turnover_cohort_{month_rr_num}_{cust_type}"] = np.NaN
                    rr_freq_df[f"AOV_cohort_{month_rr_num}_{cust_type}"] = np.NaN

            selected_uncb_results_df = selected_uncb_results_df.drop(
                columns=[
                    f"unique_customers_{cust_type}"
                    for cust_type in ["act", "react", "new", "total"]
                ]
            ).merge(
                rr_freq_df.drop(
                    columns=[
                        f"unique_customers_{cust_type}"
                        for cust_type in ["act", "react", "new", "total"]
                    ]
                ),
                how="left",
                on=["year", "month"],
            )

            selected_uncb_results_df = selected_uncb_results_df[
                selected_uncb_results_df["year"] >= 2016
            ].reset_index(drop=True)

        selected_uncb_results_df.drop(
            columns=[
                f"unique_customers_{cust_type}"
                for cust_type in ["act", "react", "new", "total"]
            ],
            inplace=True,
            errors="ignore",
        )

        if (
            (
                selected_uncb_results_df[
                    [_ for _ in selected_uncb_results_df.columns if "freq" in _]
                ]
                < 1
            )
            .any()
            .any()
        ):
            raise Exception("Some freq < 1")

        selected_uncb_results_df = selected_uncb_results_df[
            selected_uncb_results_df["year"] >= 2016
        ].reset_index(drop=True)

        all_combination_list.append(selected_uncb_results_df.copy())

    return all_combination_list
