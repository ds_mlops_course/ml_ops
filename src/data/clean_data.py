import pandas as pd
import click
from datetime import datetime
import numpy as np
from rr_preprocessing import create_unique_combination_column, drop_na_and_nunique, calculate_all

MIN_AREA = 10  # Outlier range for floor area
MAX_AREA = 300

MIN_PRICE = 1_000_000  # Outlier range for price
MAX_PRICE = 100_000_000

MIN_KITCHEN = 3  # Outlier range for kitchen area
MAX_KITCHEN = 70


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def clean_data(input_path: str, output_path: str):
    """Function removes excess columns and enforces
    correct data types.
    :param input_path: Path to read filtered by region DataFrame
    :param output_path: Path to save cleaned DataFrame
    :return:
    """
    df = pd.read_csv(input_path)
    reports_granulation_list = [
        {
            'unique_combination_list': ['country'],
            'calculate_rr': False
        },
        #     {
        #         'unique_combination_list':['country', 'skin_top'],
        #         'calculate_rr':True
        #     },
        #     {
        #         'unique_combination_list':['country', 'skin_top', 'is_b2b'],
        #         'calculate_rr':True
        #     },
        #     {
        #         'unique_combination_list':['country', 'project'],
        #         'calculate_rr':False
        #     },
        # #     {
        # #         'unique_combination_list':['country', 'project','skin_new'],
        # #         'calculate_rr':False
        # #     },
        #     {
        #         'unique_combination_list':['country', 'project', 'skin_top'],
        #         'calculate_rr':False
        #     },
        #     {
        #         'unique_combination_list':['country', 'project', 'skin_top', 'is_b2b'],
        #         'calculate_rr':False
        #     },
        #     {
        #         'unique_combination_list':['country', 'project', 'skin_top', 'channel_traffic', 'is_b2b'],
        #         'calculate_rr':False
        #     },
        #     {
        #         'unique_combination_list':['country', 'project', 'channel_traffic'],
        #         'calculate_rr':False
        #     },
    ]

    all_granulation_result_list = []
    for granulation_dict in reports_granulation_list:
        unique_combination_list = granulation_dict['unique_combination_list']
        calculate_rr = granulation_dict['calculate_rr']

        unique_combination_df = df[unique_combination_list].drop_duplicates().reset_index(drop=True)
        unique_combination_df['unique_combination'] = \
            create_unique_combination_column(unique_combination_df, unique_combination_list)

        unique_combination_df

        df = df.drop(columns=['unique_combination'], errors='ignore').merge(
            unique_combination_df,
            how='left',
            on=unique_combination_list
        )
        data_rr = calculate_all(df, unique_combination_df, unique_combination_list, calculate_rr)
        all_combination_df = pd.concat(data_rr).reset_index(drop=True)
        all_combination_df['report_granulation'] = '|'.join(unique_combination_list)
        all_combination_df = all_combination_df.drop(columns=unique_combination_list,
                                                     errors='ignore')
        all_granulation_result_list.append(all_combination_df.copy())

    all_granulation_result_df = pd.concat(all_granulation_result_list)
    all_granulation_result_df['report_date'] = str(datetime.now())[:19]

    all_granulation_result_df['country'] = all_granulation_result_df['unique_combination'].apply(
        lambda row: row.split('|')[0])
    all_granulation_result_df['B2B_B2C'] = all_granulation_result_df['unique_combination'].apply(
        lambda row: row.split('|')[1] if '|' in row else np.NaN).astype(object)
    all_granulation_result_df['project'] = all_granulation_result_df['unique_combination'].apply(
        lambda row: row.split('|')[2] if '|' in row else np.NaN).astype(object)
    all_granulation_result_df['skin'] = all_granulation_result_df['unique_combination'].apply(
        lambda row: row.split('|')[3] if '|' in row else np.NaN).astype(object)
    all_granulation_result_df['channel_traffic'] = all_granulation_result_df['unique_combination'].apply(
        lambda row: row.split('|')[4] if '|' in row else np.NaN).astype(object)

    all_granulation_result_df.to_csv(output_path, index=False)


if __name__ == "__main__":
    clean_data()
